# lego-instruction

A small script to fetch and open the lego instructions by product_number.

Requires
* jq
* kdialog

## Installation

My drive was to have an easy way for my child to look up instruction, which they do for which set interests them (typically based on paper instructions they have). The easy process is: tap on the lego button, enter the number, press enter.

1. Place the lego.svg and lego-instruction.sh in /opt/lego-instruction/
1. Make the script executable: chmod +x /opt/lego-instruction/lego-instruction
1. Create a menu entry via GUI, using /opt/lego-instruction/lego-instruction binary and the svg file as icon. Use /opt/lego-instruction/ as work dir.
1. You may add the entry to the desktop or panel for most easy access :)
